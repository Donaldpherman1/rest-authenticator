import { createRequest, createResponse } from "node-mocks-http";
import {
  RestAuthenticator,
  KeycloakRestAuthenticator,
  AuthenticationRequest,
  AuthenticationResponse
} from "..";
import uuid from "uuid";

class NotImplementedAuth implements KeycloakRestAuthenticator {
  processNew(
    req: AuthenticationRequest
  ): Promise<AuthenticationResponse | null> {
    throw new Error("Method not implemented.");
  }
  processInteraction(
    req: AuthenticationRequest
  ): Promise<AuthenticationResponse | null> {
    throw new Error("Method not implemented.");
  }
  processInterruption(
    req: AuthenticationRequest
  ): Promise<AuthenticationResponse | null> {
    throw new Error("Method not implemented.");
  }
}

function createMinimumAuthenticationRequest(): AuthenticationRequest {
  return {
    authenticator: "test",
    mode: "authenticate",
    execution: uuid(),
    authSession: {
      action: "AUTHENTICATE",
      authNote: {},
      clientNotes: {},
      clientScopes: [],
      executionStatus: {},
      protocol: "openid-connect",
      tabId: "random",
      userSessionNotes: {}
    },
    client: {
      attributes: {},
      baseUrl: "",
      clientId: "client",
      description: "",
      id: "client_id",
      name: "client"
    },
    connection: {
      address: "",
      host: "",
      port: 0
    },
    httpRequest: {
      formParams: {},
      headers: {},
      uri: "",
      uriPath: "",
      uriQueryParams: {}
    },
    realm: {
      attributes: {},
      id: "realm_id",
      name: "realm"
    }
  };
}

describe("Basic testing", () => {
  it("should fail because not implemented", () => {
    const handler = RestAuthenticator.declare(
      "/test",
      new NotImplementedAuth()
    );
    const body = createMinimumAuthenticationRequest();
    const req = createRequest({
      url: "http://host/test",
      method: "POST",
      body: body
    });
    const res = createResponse();
    handler(req, res, (err: any) => {
      //console.log(res._getStatusCode());
      expect(err).not.toBeDefined();
    });
    //console.log("Post call", res._getStatusCode(), res._getJSONData());
    expect(res._getStatusCode()).toEqual(500);
  });
});
