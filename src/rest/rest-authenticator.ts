import express, {
  Router,
  RequestHandler,
  Request,
  Response,
  NextFunction
} from "express";
import DebugLib from "debug";
const debug = DebugLib("rest-authenticator:express");
//import logger from "../logger";

import {
  KeycloakRestAuthenticator,
  AuthenticationRequest,
  AuthenticationResponse
} from "../domain/types";
import { getAuthenticationRequest, InvalidRequest } from "../rest/rest-helpers";
import KeycloakScriptPackager from "../deploy/keycloak-packager";

/** Class to handle the http requests in the Express framework */
export class RestAuthenticator {
  private authenticator: KeycloakRestAuthenticator;
  private router: Router;
  /**
   * Create a new instance of the Express handler (a Router)
   * @param restPath the path that will be accepting the POST request
   * @param authenticator the implementation of the authenticator
   */
  constructor(restPath: string, authenticator: KeycloakRestAuthenticator) {
    if (!authenticator) {
      throw new Error("Missing a valid keycloak authenticator");
    }
    this.authenticator = authenticator;

    this.router = express.Router();
    this.router.post(restPath, this.handler.bind(this));
  }

  /** returns the Router as an Express request handler. It respond to POST /path */
  mw(): RequestHandler {
    return this.router;
  }
  private async handler(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    try {
      debug("received body: %s", JSON.stringify(req.body));
      const request = getAuthenticationRequest(req);
      let response: AuthenticationResponse | null = null;
      if (request.mode == "authenticate") {
        response = await this.authenticator.processNew(request);
      } else if (request.authSession.authNote.callCount == 2) {
        response = await this.authenticator.processInteraction(request);
      } else {
        response = await this.authenticator.processInterruption(request);
        if (response) {
          response.fork = true;
        }
      }
      if (response) {
        if (!response.authNote) {
          response.authNote = {};
        }
        // force to 1 as a final response to make the difference between the first call, normal interaction call or interruption call
        response.authNote.callCount = 1;
        debug("Returning response %s", JSON.stringify(response));
        res.status(200).json(response);
      } else {
        debug("Unexpected null response");
        res.status(520).end();
      }
    } catch (error) {
      const message = error.message ? error.message : error.toString();
      if (error instanceof InvalidRequest) {
        debug("Invalid request: %s", message);
        res.status(400).json({ cause: message });
      } else {
        debug(
          "Error while processing the authentication request: %s",
          error.toString()
        );
        res.status(500).json({ cause: message });
      }
    }
  }

  /**
   * static wrapper to create an Express middleware and at the same time optionally record it in the Keycloak packager
   * @param restPath the path for the handler
   * @param implementation the authenticator implementation
   * @param packager the instance of the Keycloak packager with which the endpoint should be registered
   */
  static declare(
    restPath: string,
    implementation: KeycloakRestAuthenticator,
    packager?: KeycloakScriptPackager
  ): RequestHandler {
    const auth = new RestAuthenticator(restPath, implementation);
    if (packager) {
      packager.addRestAuthenticator(restPath);
    }
    return auth.mw();
  }
}
