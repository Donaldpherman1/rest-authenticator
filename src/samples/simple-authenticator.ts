import {
  KeycloakRestAuthenticator,
  AuthenticationRequest,
  AuthenticationResponse,
  AuthNoteModel
} from "../domain/types";

export class SimpleAuthenticator implements KeycloakRestAuthenticator {
  async processNew(
    req: AuthenticationRequest
  ): Promise<AuthenticationResponse | null> {
    if (!req.user) {
      return {
        failure: "unknownUser"
      };
    }
    if (req.user?.attributes.PASSED_SECRET) {
      return {
        userSessionNotes: {
          hasPassedSecret: "automatically"
        }
      };
    }
    return {
      challenge: {
        formName: "login-password.ftl",
        message: "Enter the secret please"
      }
    };
  }
  async processInteraction(
    req: AuthenticationRequest
  ): Promise<AuthenticationResponse | null> {
    // SimpleAuthenticator (interaction) received { password: [ 'BLA' ] }
    const formParams = req.httpRequest.formParams;
    if (formParams.password && formParams.password.length == 1) {
      if (formParams.password[0] == "BLA") {
        return {
          userAttributes: {
            PASSED_SECRET: "well done"
          },
          userSessionNotes: {
            hasPassedSecret: "form"
          }
        };
      }
    }
    return {
      failure: "invalidCredentials",
      challenge: {
        formName: "login-password.ftl",
        message: "Try again to enter the secret"
      }
    };
  }

  async processInterruption(
    req: AuthenticationRequest
  ): Promise<AuthenticationResponse | null> {
    throw new Error("Method not implemented.");
  }
}
